<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WarehouseController;
use App\Models\Warehouse;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix'=>'user',['middleware'=>['auth','check:1,2,3']]],function(){
    Route::get('export', [UserController::class,'export'])->name('export');
    Route::get('Product/{product}', [ProductController::class,'list'])->name('product.list');
    Route::get('sidebar', [UserController::class,'list'])->name('sidebar');
    Route::get('export_type', [TypeController::class,'export'])->name('type_export');
});

Route::group(['middleware'=>['auth','role:0']],function(){
    Route::post('sendMail/{user}' ,[UserController::class,'sendMail'])->name('sendMail');
    Route::resource('admin', UserController::class);
    Route::resource('warehouse', WarehouseController::class)->names('warehouse');
    Route::resource('type', TypeController::class)->names('type');
    Route::resource('product', ProductController::class)->names('product');
    
});