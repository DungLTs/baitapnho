<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>
    <title>Document</title>

</head>

<body>
    <div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh ;">
        <form  action="{{route('admin.update',$user->id) }}" class="border shadow p-3 rounded" method="POST"
            style="width:400px">
            @method('PUT')
            @csrf
            <h1 class="text-center p3" style="font-size:15px ;">CHANGE PASSWORD</h1>
            <div class="mb-3">
                <label for="email">Email</label>
                <input type="email" id="email" class="form-control" id="email" name="email">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="password">Password</label>
                <input type="password" id="password" class="form-control" id="password" name="password">
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="confirm">Confirm password</label>
                <input type="confirm" id="confirm_password" class="form-control" id="confirm"
                    name="password_confirmation">
                @error('password_confirmation')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <input type="id" hidden value="{{ $user->id }}" class="form-control" id="id" name="id">



            <button type="submit" name="forgot" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>

</html>
