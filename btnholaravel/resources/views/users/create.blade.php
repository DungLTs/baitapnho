@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('admin.store') }}" method="POST">
            @csrf
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">username</label>
                <input type="text" class="form-control" name="username">
                @error('username')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Password</label>
                <input type="text" class="form-control" name="password">
                @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="cars">Chức vụ</label>
                <select id="agileinfo-nav_search" name="role" class="border form-control" >
                    <option>----Chọn----</option>
                   @foreach ($warehouses as $warehouse) 
                    <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                    @endforeach
                </select>
                @error('role')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            
            <button type="submit" class="ml-5 btn btn-primary">Submit</button>
        </form>
     
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
