@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>User Manager</h1>
@stop

@section('content')
    <div class="card">

        <!-- /.card-header -->
        <div class="card-body">
            <div class="p-2 bd-highlight"><a class="btn btn-success" style="text-decoration: none; color: #ffff;"
                    href="{{ route('admin.create') }}">Add</a></div>
            <div class="p-2 bd-highlight">
                <form action="{{route('export')}}" method="Get">
                    @csrf
                    <button type="submit" class="btn btn-info" name="export_exel">Export excel</button>
                </form>
            </div>

            <table id="example2" class="table table-bordered table-hover">
                <tr>
                    @php $i = 0; @endphp
                    <th>STT</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Chức vụ</th>
                    <th style="width:20px;">Xóa</th>
                    
                </tr>  
                @foreach ($users as $user)
                    <tr @if ($loop->first) hidden @endif>
                        <td>{{ $i++ }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->warehouse->name ?? 'admin' }}</td>
                        <td>
                            <form action="{{ route('admin.destroy', $user->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger" type="submit">Xóa</button>
                            </form>
                        </td>
                        
                        <td>
                            <form action="{{ route('sendMail',$user->username) }}" method="POST">
                                @csrf
                                <input type="text" hidden class="check" name="check" value="{{ $user->email }}">
                                <input type="hidden" name="name" value="{{ $user->username }}">
                                <button type="submit" class=" btn btn-secondary">Reset password</button>
                            </form>
                        </td>

                    </tr>
                @endforeach


            </table>

        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
