@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>San pham</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="p-2 bd-highlight"><a href="{{ route('type.create') }}" class="btn btn-success">Add</a></div>
            <div class="p-2 bd-highlight">
                <form action="{{ route('type_export') }}" method="GET">
                    @csrf
                    <button class="btn btn-info" style="text-decoration: none; color: #ffff;" type="submit"
                        name="export_exel">Export excel</button>
                </form>
            </div>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    @php
                        $i = 1;
                    @endphp
                    <tr>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Loại sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Loại</th>
                        <th>Ngày</th>
                        <th style="width:20px;"></th>
                        <th style="width:20px;"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($products as $product)
                        @foreach ($product->types as $type)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $type->name }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $type->amount }}</td>
                                <td>{{ $type->type_date }}</td>
                                <td>{{ $type->create_at }}</td>
                                <td>
                                    <form action="{{ route('type.destroy', $type->id) }}"method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-warning">Xóa</button>
                                    </form>

                                </td>
                                <td><a class="btn btn-danger" href="{{ route('type.edit', $type->id) }}">Sửa</a></td>

                            </tr>
                        @endforeach
                    @endforeach



            </table>
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
