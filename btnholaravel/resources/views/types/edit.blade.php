@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{route('type.store',$type->id)}}" method="POST">
            @csrf
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" value="{{$type->name}}" name="name">
                <input type="text" hidden class="form-control" value="{{$type->id}}" name="id">
                @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>

            <div class="w-50 ml-5 pt-2 cart 6 table table-hover"> <label>Kiểu:</label><br>
                <select id="" name="type_date" class="form-select form-control  form-select-sm">

                    <option @selected($type->type_date == 'Nhập') value="Nhập">Nhập</option>
                    <option @selected($type->type_date == 'Xuất') value="Xuất">Xuất</option>

                </select>
                @error('type_date')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Số lượng</label>
                <input type="text" class="form-control" value="{{$type->amount}} "name="amount" >
                @error('amount')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Ngày</label>
                <input type="datetime" class="form-control" value="{{$type->create_at}}" name="create_at">
                @error('create_at')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="cars">Kho:</label><br>
                <select id="agileinfo-nav_search" name="product_id" class="form-control  form-select form-select-lg mb-3">
                    @foreach ($products as $product)
                        <option @selected($product->id == $type->product_id) value="{{ $product->id }}">{{ $product->product_name }}
                        </option>
                    @endforeach
                </select>
                @error('product_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
            </div>
            <button type="submit" name="edit" class="w-50 ml-5 pt-2  btn btn-primary">Submit</button>
        </form>
        <!-- /.card-header -->
        <div class="card-body">

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
