@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{ route('type.store') }}" method="POST">
            @csrf
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Số lượng</label>
                <input type="number" class="form-control" name="amount">
                @error('amount')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover"> <label>Kiểu:</label><br>
                <select id="" name="type_date" class="form-select form-select-sm form-control">
                    <option value="">----Chọn Kiểu----</option>

                    <option value="Nhập">Nhập</option>
                    <option value="Xuất">Xuất</option>

                </select>
                @error('type_date')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Ngày</label>
                <input type="date" class="form-control" name="create_at">
                @error('create_at')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="w-50 ml-5 pt-2 cart 6 table table-hover"> <label for="cars">Loại sản phẩm:</label><br>

                <select id="agileinfo-nav_search" name="product_id" class="form-select form-select-lg mb-3 form-control">
                    <option value="">----Chọn loại sản phẩm---- </option>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                    @endforeach
                </select>
                @error('product_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="ml-5 btn btn-primary">Submit</button>
        </form>
        <!-- /.card-header -->
        <div class="card-body">

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
