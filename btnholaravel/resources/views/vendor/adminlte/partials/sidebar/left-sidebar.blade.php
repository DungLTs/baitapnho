<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    @if (config('adminlte.logo_img_xl'))
        @include('adminlte::partials.common.brand-logo-xl')
    @else
        @include('adminlte::partials.common.brand-logo-xs')
    @endif

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="pt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
             
                  <li class="nav-item">
                    <a href="" class="nav-link">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Quản lý
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{route('admin.index')}}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Quản lý user</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{route('warehouse.index')}}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Quản lý Kho hàng</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{route('product.index')}}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Quản lý Loại sản phẩm</p>
                        </a>
                      </li>
    
                    </ul>
    
                  </li>
    
                  @foreach($warehouses as $warehouse)
                    <li class="nav-item">
                      <a href="{{route('product.list',$warehouse->id)}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                          {{$warehouse->name}}
                          
                        </p>
                      </a>
                     
    
                        @endforeach
    
                            
                          
                    </li> 
           
              </ul>
              </li>
            
    
    
    
    
    
          </ul>
        </nav>
    </div>

</aside>
