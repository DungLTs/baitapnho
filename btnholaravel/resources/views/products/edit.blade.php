@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{route('product.store',$product->id)}}" method="POST">
            @csrf
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <input hidden type="text" name="id" value="{{$product->id}}">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" value="{{$product->product_name}}" name="name">
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover"> <label for="cars">Kho:</label>
                <select id="agileinfo-nav_search" name="warehouse_id" class="form-select form-select-lg mb-3 form-control"
                    required="">
                    <option value="">----Chọn kho----</option>
                    @foreach ($warehouses as $warehouse)
                        <option @selected($warehouse->id==$product->warehouse_id) value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                    @endforeach


                </select>
            </div>

            <button type="submit" class="ml-5 btn btn-primary">Submit</button>
        </form>
        <!-- /.card-header -->
        <div class="card-body">

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
