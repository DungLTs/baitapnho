@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">
        <form action="{{route('product.store')}}" method="POST">
            @csrf
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="w-50 ml-5 pt-2 cart 6 table table-hover"> <label for="cars">Kho:</label>
                <select id="agileinfo-nav_search" name="warehouse_id" class="form-select form-select-lg mb-3 form-control"
                    >
                    <option value="">----Chọn kho----</option>
                    @foreach ($warehouses as $warehouse)
                        <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                    @endforeach


                </select>
                @error('warehouse_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>

            <button type="submit" class="ml-5 btn btn-primary">Submit</button>
        </form>
        <!-- /.card-header -->
        <div class="card-body">

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
