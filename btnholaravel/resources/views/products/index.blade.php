@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="card">

        <!-- /.card-header -->
        <div class="card-body">
            <div class="p-2 bd-highlight">
                <a class="btn btn-success" href="{{ route('product.create') }}">Add</a>
            </div>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    @php
                        $i = 1;
                    @endphp
                    <tr>
                        <th>STT</th>
                        <th>Name</th>
                        <th>Kho</th>
                        <th style="width:20px;"> </th>
                        <th style="width:20px;"> </th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($warehouses as $warehouse)
                        @foreach ($warehouse->products as $product)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $warehouse->name }}</td>
                                <td><a class="btn btn-danger" href="{{route('product.edit',$product->id)}}">Sửa</a> </td>
                                <td>
                                    <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-warning">Xóa</button>
                                </td>
                                </form>

                            </tr>
                        @endforeach
                    @endforeach
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
