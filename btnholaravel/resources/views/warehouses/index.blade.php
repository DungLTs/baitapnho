@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Quản lý kho hàng</h1>
@stop

@section('content')

    <div class="card">

        <div class="card-body">
            <a class="btn btn-primary" href="{{ route('warehouse.create') }}">Add</a>
            <table class="table table-bordered">
                
                <thead>
                    <tr>
                        <th style="width: 10px">STT</th>
                        <th>Name</th>
                        <th style="width: 40px"></th>
                        <th style="width: 40px"></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                     $stt=1;   
                    @endphp
                    @foreach ($warehouses as $warehouse)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$warehouse->name}}</td>

                            <td><a class="btn btn-primary" href="{{ route('warehouse.edit', $warehouse->id) }}">Edit</a></td>
                            <td><form  action="{{route('warehouse.destroy',$warehouse->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" >Delete</button>
                            </form></td>
                        </tr>
                    @endforeach


                </tbody>
            </table>
        </div>

        <div class="card-footer clearfix">

        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
