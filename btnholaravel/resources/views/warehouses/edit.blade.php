@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Quản lý kho hàng</h1>
@stop

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">CREATE OR UPDATE</h3>
        </div>

        <div class="card-body">
            <form action="{{ route('warehouse.store', $warehouse->id) }}" method="POST">

                @csrf
                <div class="w-50 ml-5 pt-2 cart 6 table table-hover">
                    <input class="form-control" type="text" value="{{ $warehouse->id }}" hidden name="id">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" value="{{ $warehouse->name }}" name="name">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="ml-5 btn btn-primary">Submit</button>
            </form>
        </div>

        <div class="card-footer clearfix">

        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
