<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class UsersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $i = 1;
    public function collection()
    {
        $user = User::with('warehouse')->get();
        return $user;
    }
    public function map($user): array
    {

        return [
            $this->i++,
            $user->username,
            $user->email,
            $user->warehouse->name ?? 'admin',

        ];
    }

    public function headings(): array
    {

        return [
            "Stt",
            "username",
            "email",
            "vitri",
        ];
    }
    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'UTF-8',
            'use_bom' => true,
        ];
    }
}
