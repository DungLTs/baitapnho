<?php

namespace App\Exports;

use App\Models\Product;
use App\Models\Type;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TypeExport implements FromCollection, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    private $i = 1;
    public function collection()
    {
        $products = Type::all();
      
        return $products;
    }
    public function map($product): array
    {
        
        return
            [
                $this->i++,
                $product->name,
                $product->amount,
                $product->product_id,
                $product->type_date,
                $product->create_at,
            ];
    }
    public function headings(): array
    {

        return [
            "Stt",
            "Ten san pham",
            "So luong",
            "Loai san pham",
            "Loai",
            "Ngay",


        ];
    }
    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'UTF-8',
            'use_bom' => true,
        ];
    }
}
