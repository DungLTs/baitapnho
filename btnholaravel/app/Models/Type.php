<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Type extends Model
{
    use HasFactory;
    protected $fillable=[
        'product_id',
        'name',
        'type_date',
        'amount',
        'create_at',
    ];
    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
