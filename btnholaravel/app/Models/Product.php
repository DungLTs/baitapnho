<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Type;
use App\Models\Warehouse;

class Product extends Model
{
    use HasFactory;
    protected $fillable=[
        'product_name',
        'warehouse_id',
    ];
    public function types()
    {
        return $this->hasMany(Type::class,'product_id','id');
    }
    public function warehouse(){
        return $this->belongsTo(warehouse::class,'warehouse_id','id');
    }
}
