<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Warehouse extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
    ];
    public function products(){
        return $this->hasMany(Product::class,'warehouse_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'id','role');
    }
}
