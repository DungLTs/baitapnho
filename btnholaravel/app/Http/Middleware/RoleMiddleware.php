<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Models\Warehouse;
use Closure;
use Illuminate\Http\Request;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next,$name)
    {
    
    
        $admin=auth()->user()->role;
        if($admin!=$name){
            abort(404);
        } 
        return $next($request);
    }
}
