<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next,...$check)
    {
        dd(121);    
        dd(auth()->user()->role);
        $id=auth()->user()->role;
        if(!in_array($id,$check)){
            abort(404);
        }
        return $next($request);
    }
}
