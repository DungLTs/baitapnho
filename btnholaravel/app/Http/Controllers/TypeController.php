<?php

namespace App\Http\Controllers;

use App\Exports\TypeExport;
use App\Http\Requests\TypeRequest;
use App\Models\Product;
use App\Models\Type;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::all();
        $products = Product::with('types')->get();
        return view('types.index', compact('products','warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouses = Warehouse::all();
        $products = Product::all();
        return view('types.updateorcreate', compact('products','warehouses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeRequest $request)
    {
        $warehouseid=Product::where('id',$request->product_id)->first()->warehouse_id;
        $types = Type::find($request->id);
        $soluong = $request->amount;
        if ($request->get('type_date') == 'Xuất') {
            $soluong = ($types->amount - $request->amount);
        }
        Type::updateOrCreate(
            ['id' => $request->id],
            [
                'name' => $request->name,
                'product_id' => $request->product_id,
                'type_date' => 'Nhập',
                'amount' => $soluong,
                'create_at' => $request->create_at
            ] 
        );
        return redirect()->route('product.list',$warehouseid);

       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        $warehouses = Warehouse::all();
        $products = Product::all();
        return view('types.edit', compact('products', 'type','warehouses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(TypeRequest $request, Type $type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $delete = Type::find($id);
        $delete->delete();
        return redirect()->back();
    }
    public function export()
    {
        return Excel::download(new TypeExport, 'Types.xlsx');
    }
}
