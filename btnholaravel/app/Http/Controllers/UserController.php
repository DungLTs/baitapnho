<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use app\Exports\UserExel;
use App\Exports\UsersExport;
use App\Models\Product;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('warehouse')->get();
        $warehouses=Warehouse::all();
        return view('users.index', compact('users','warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouses = Warehouse::all();
        return view('users.create', compact('warehouses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        User::Create($request->all());
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, User $user)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
        ]);
        $user = User::find($id);
        User::find($request->id)->update(
            [
                'first_login' => 0,
                'username' => $user->username,
                'email' => $request->email,
                'password' => $request->password,
            ]
        );
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('admin.index');
    }
    public function sendMail(Request $request,$user)
    {
        $user = User::where('username', $request->name)->first();
        $password = substr(md5(rand(1, 99999)), 0, 8);
       
        User::find($user->id)->update(
            [
                'first_login'=>1,
                'password'=>$password,
            ]
        );
        Mail::send(
            'users.email',
            compact(['user', 'password']),
            function ($message) use ($user) {
                $message->to($user->email, $user->name);
                $message->subject('Reset mật khẩu');
            }
        );
        return redirect()->route('admin.index');
    }
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    public function list()
    {
        $products=Product::all();
        $warehouses=Warehouse::all();
        return view('vendor.adminlte.sidebar.left-sidebar',compact('warehouses','products'));

    }
}
